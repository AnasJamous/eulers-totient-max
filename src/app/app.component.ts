import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = "Euler's Totient Maximum ";
  eulerTotientMax;

  // @params - number --- return max Euler Totient
  EulerTotientMax = (num) => {
    // list of numbers for general limitations
    let primesNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 21, 23, 29, 31];
    // Initial Value for the max Totient
    let maxEuler = 1;
    // Init Counter for the iteration
    let count = 0;
    while (primesNumbers[count] * maxEuler <= num)
      maxEuler *= primesNumbers[count++];
    return maxEuler;
  };

  onEnter = (value, event) => {
    event.target.value = '';

    value > 0
      ? (this.eulerTotientMax = this.EulerTotientMax(value))
      : (this.eulerTotientMax = 'Please enter a positive number :)');
  };
}
